# Mosaic Collage Generator

The Mosaic Collage Generator is a command-line tool written in Rust that creates a collage from a directory of image files. It shuffles the images and arranges them in a grid to generate a mosaic collage.

## Features

- Supports JPEG and PNG image formats.
- Automatically shuffles the images for a random arrangement in the collage.


## Latest release
https://gitlab.com/DrD33M/mosaic_collage_generator/-/releases
## Usage
### output as png
Base usage:
```./mosaic_collage_generator <directory_path> <output_path> <collage_width> <collage_height>```

Generate a 1080p image:
```./mosaic_collage_generator /home/bob/Music/ ./collage.png 1920 1080```

