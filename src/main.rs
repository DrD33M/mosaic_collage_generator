use std::{
    env,
    sync::{Arc, Mutex},
    time::Instant,
};

use image::{GenericImage, ImageBuffer, RgbaImage};
use indicatif::{ProgressBar, ProgressStyle};
use rand::rngs::mock::StepRng;
use rayon::prelude::*;
use shuffle::irs::Irs;
use shuffle::shuffler::Shuffler;
use walkdir::WalkDir;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 5 {
        println!(
            "Usage: {} <directory_path> <output_path> <collage_width> <collage_height>",
            args[0]
        );
        return;
    }

    let directory_path = &args[1];
    let output_path = &args[2];
    let collage_width: u32 = match args[3].parse() {
        Ok(width) => width,
        Err(_) => {
            println!("Invalid collage width.");
            return;
        }
    };
    let collage_height: u32 = match args[4].parse() {
        Ok(height) => height,
        Err(_) => {
            println!("Invalid collage height.");
            return;
        }
    };
    let mut image_paths = find_image_files(directory_path);

    if image_paths.is_empty() {
        println!("No image files found.");
        return;
    }

    let start_time = Instant::now();
    let num_images = image_paths.len();

    let progress_bar = ProgressBar::new(num_images as u64);
    progress_bar.set_style(
        ProgressStyle::default_bar()
            .template("[{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})")
            .unwrap()
            .progress_chars("##-"),
    );
    let mut rng = StepRng::new(2, 13);
    let mut irs = Irs::default();
    irs.shuffle(&mut image_paths, &mut rng).unwrap();
    let collage =
        generate_mosaic_collage(&image_paths, collage_width, collage_height, &progress_bar);
    progress_bar.finish_and_clear();
    let duration = start_time.elapsed();
    println!("Collage generation completed in {:?}", duration);

    save_collage(&collage, output_path).expect("Failed to save collage.");
}

fn find_image_files(dir: &str) -> Vec<String> {
    let mut image_paths = vec![];

    for entry in WalkDir::new(dir).into_iter().filter_map(|e| e.ok()) {
        if entry.file_type().is_file() {
            if let Some(ext) = entry.path().extension() {
                let ext_str = ext.to_string_lossy().to_lowercase();
                if ext_str == "jpg" || ext_str == "jpeg" || ext_str == "png" {
                    if entry
                        .path()
                        .to_string_lossy()
                        .to_lowercase()
                        .contains("cover.")
                        || entry
                            .path()
                            .to_string_lossy()
                            .to_lowercase()
                            .contains("folder.")
                    {
                        println!("Processing {}", entry.path().to_string_lossy());
                        image_paths.push(entry.path().to_string_lossy().into());
                    }
                }
            }
        }
    }

    image_paths
}

fn generate_mosaic_collage(
    image_paths: &[String],
    image_collage_width: u32,
    image_collage_height: u32,
    progress_bar: &ProgressBar,
) -> RgbaImage {
    let num_images = image_paths.len();
    let tile_size = ((image_collage_width * image_collage_height) as f32 / num_images as f32)
        .sqrt()
        .ceil() as u32;
    let collage_width = (image_collage_width / tile_size) * tile_size;
    let collage_height = (image_collage_height / tile_size) * tile_size;

    let collage = Arc::new(Mutex::new(ImageBuffer::new(collage_width, collage_height)));

    image_paths.par_iter().enumerate().for_each(|(i, path)| {
        match image::open(path) {
            Ok(img) => {
                let resized_img = img.thumbnail_exact(tile_size, tile_size);
                let x = (i as u32 % (collage_width / tile_size)) * tile_size;
                let y = (i as u32 / (collage_width / tile_size)) * tile_size;

                let mut collage_lock = collage.lock().unwrap();
                if let Err(err) = collage_lock.copy_from(&resized_img, x, y) {
                    println!("Failed to copy {} to collage: {}", path, err);
                }
            }
            Err(err) => {
                println!("Failed to open image {}: {}", path, err);
            }
        }
        progress_bar.inc(1);
    });

    Arc::try_unwrap(collage)
        .unwrap()
        .into_inner()
        .expect("Failed to unwrap the collage mutex")
}

fn save_collage(collage: &RgbaImage, output_path: &str) -> Result<(), image::ImageError> {
    collage.save(output_path)
}
